Dim StartTime,ExitTime,FinalHourDifference,FinalMinuteDifference,SecondDifference,FinalSecondDifference
StartTime=WScript.Arguments.Item(0)
ExitTime=WScript.Arguments.Item(1)
SecondDifference = DateDiff("s",  StartTime , ExitTime)
FinalHourDifference=Int(SecondDifference/3600)
FinalMinuteDifference=Int(SecondDifference/60 - FinalHourDifference*60)
FinalSecondDifference=SecondDifference/60 - FinalHourDifference*60 - FinalMinuteDifference
FinalSecondDifference=Int(FinalSecondDifference*60)
WScript.StdOut.WriteLine(FinalHourDifference & ":" & FinalMinuteDifference & ":" & FinalSecondDifference)
