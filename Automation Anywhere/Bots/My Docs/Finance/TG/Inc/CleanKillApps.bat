REM ---------- # ---------- Kill Target Systems[Citrix Workspace, Citrix ICA Client]
taskkill/F /IM SelfService.exe
taskkill/F /IM wfica32.exe
REM ---------- # ---------- Kill Browsers[Internet Explorer, Chrome, Firefox]
Taskkill /F /IM iexplore.exe
REM Taskkill /F /IM Chrome.exe
Taskkill /F /IM firefox.exe
REM ---------- # ---------- Kill Office Apps[Word, Excel, Teams]
Taskkill /F /IM WinWord.exe
Taskkill /F /IM Excel.exe
REM Taskkill /F /IM Teams.exe
REM ---------- # ---------- Delete Temp Files
Del /S /F /Q %TEMP%
Del /S /F /Q %TEMP%\
Del /S /F /Q %TEMP%\*.*
